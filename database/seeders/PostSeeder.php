<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            'content' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi fugit dolorem molestiae in quas! Incidunt ea expedita quis distinctio repudiandae!',
            'status' => 'pendding',
            'user_id' => 2,
        ]);
        DB::table('posts')->insert([
            'content' => 'Quas cum nesciunt dolores culpa? Voluptates qui alias quo illo.',
            'status' => 'pendding',
            'user_id' => 2,
        ]);

    }
}
