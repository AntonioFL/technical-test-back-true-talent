<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// ====================================================== //
// ======================= PUBLIC ======================= //
// ====================================================== //

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
Route::post('/password/email', [AuthController::class, 'sendPasswordResetLinkEmail'])->name('password.email');
Route::post('/password/reset', [AuthController::class, 'resetPassword'])->name('password.reset');

// ====================================================== //
// ====================== PRIVATE ======================= //
// ====================================================== //

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/user', [AuthController::class, 'infoUser']);
    Route::get('/logout', [AuthController::class, 'logout']);

    Route::resource('posts', PostController::class)->except([
        'create', 'edit',
    ])->middleware(['roles:admin|guest']);

    Route::resource('comments', CommentController::class)->except([
        'create', 'edit',
    ])->middleware(['roles:admin|guest']);

});
