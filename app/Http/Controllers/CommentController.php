<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCommentRequest;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{

    /**
     * Store a new comment
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCommentRequest $request)
    {

        $commentCreated = Comment::create($request->all());

        return response()->json([
            'message' => 'Comentario creado',
            'comment' => $commentCreated->load('user'),
        ], 201);

    }

}
