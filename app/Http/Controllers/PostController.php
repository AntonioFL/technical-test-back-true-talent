<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePostRequest;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Shows a list of posts with comments and related users
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role_id == 1) {
            $posts = Post::with(['comments.user', 'user'])->get();
        } else {
            $posts = Post::with(['comments.user', 'user'])->where('user_id', Auth::user()->id)->get();
        }

        return response()->json([
            'message' => 'Lista de posts',
            'posts' => $posts,
            'user_id' => Auth::user()->id,
        ], 200);
    }

    /**
     * Store a new post and returns it with comments and related users
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request)
    {

        $postCreated = Post::create($request->all());
        $postCreated->status = 'pendding';

        return response()->json([
            'message' => 'Post creado',
            'post' => $postCreated->load(['comments.user', 'user']),
        ], 201);

    }

    /**
     * Display the specified post  with comments and related users
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::with(['comments', 'user'])->find($id);

        return response()->json([
            'message' => 'Post creado',
            'post' => $post,
        ], 200);
    }

    /**
     * Update the specified post.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        $post->status = $request->status;
        $post->save();

        return response()->json([
            'message' => 'Post actualizado',
        ], 200);
    }

}
