<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Roles
{
    /**
     * Allowed roles are validated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $roles)
    {
        $user = Auth::user();
        $available_roles = ['admin' => 1, 'guest' => 2];
        $roles = explode('|', $roles);
        $allowed_roles = [];

        foreach ($roles as $role) {
            if (array_key_exists($role, $available_roles)) {
                $allowed_roles[] = $available_roles[$role];
            }
        }

        if (!in_array($user->role_id, $allowed_roles)) {
            return response()->json([
                'message' => 'Access denied',
            ], 403);
        }

        return $next($request);
    }
}
