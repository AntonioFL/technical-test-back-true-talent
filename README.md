<p  align="center"><a  href="https://laravel.com"  target="_blank"><img  src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg"  width="400"  alt="Laravel Logo"></a></p>

# CHALLENGE - TRUE TALENT

### Funcionalidad

Esta API te permitirá publicar y administrar frases o citas, basada en dos tipos de roles de usuario (administrador e invitado).

#### Acciones del administrador.

-   Inicio de sesión.
-   Recuperar contraseña.
-   Consultar entradas recientes.
-   Rechazar o aceptar entradas recientes.
-   Comentar entradas.

#### Acciones para los invitados.

-   Registro.
-   Inicio de sesión.
-   Recuperar contraseña.
-   Publicar frases o citas (entradas)
-   Retirar entradas.
-   Comentar entradas propias o de otros invitados.

### Tecnologías

La API esta construida sobre Laravel 9 y PHP 8

### Cómo levantar el entorno

1 .- Crea un archivo `.env`, dentro, copia y pega el contenido de `.env.example`

1.2 .- Para poder enviar correos electrónicos asegurate de configurar las siguientes variables con un proveedor de correos valido. (Recomiendo utilizar https://mailtrap.io/ para simular el envio de correos))

```
MAIL_MAILER=
MAIL_HOST=
MAIL_PORT=
MAIL_USERNAME=
MAIL_PASSWORD=
MAIL_ENCRYPTION=
MAIL_FROM_ADDRESS=
MAIL_FROM_NAME=
```

2 .- La aplicación hace uso de Laravel Sails, por lo cual después de clonar el repositorio, es necesario descargar las dependencias. Asegurate de tener corriendo Docker en tu computadora.

En el directorio de la aplicación, ejecuta el siguiente comando:

```
docker run --rm \
-u "$(id -u):$(id -g)" \
-v $(pwd):/var/www/html \
-w /var/www/html \
laravelsail/php81-composer:latest \
composer install --ignore-platform-reqs
```

3 .- Para levantar el entorno (en un contenedor Docker) ejecuta el comando:

`./vendor/bin/sail up`

4 .- Ejecuta las migraciones con el siguiente comando

`./vendor/bin/sail artisan migrate`

Nota: La BD se crea en automático al ejecutar la migración, en caso de tener un error porque no se genero automáticamente, puedes hacer uso de las credenciales para la BD que se encuentran en el archivo .env/.env.explample y hacer uso de Workbench o phpMyAdmin para crear la BD, ahora puedes intentar ejecutar nuevamente las migraciones

5 .- El siguiente paso es ejecutar los seeders para cargar datos en la Base de datos
`./vendor/bin/sail artisan db:seed`

#### Importante:

Para probar el API clona y levanta el proyecto [technical-test-front-true-talent](https://laravel.com/docs/routing)

#### Credenciales del usuario administrador

```
username: admin@test.com
password: 12345678
```
